﻿// <auto-generated />
using System;
using EFPlusTest;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace EFPlusTest.Migrations
{
    [DbContext(typeof(TestContext))]
    partial class TestContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                .HasAnnotation("ProductVersion", "3.1.3")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("EFPlusTest.TestEntity", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<long?>("ChildId")
                        .HasColumnType("bigint");

                    b.Property<string>("Data")
                        .HasColumnType("text");

                    b.Property<bool>("TriggerHit")
                        .HasColumnType("boolean");

                    b.HasKey("Id");

                    b.HasIndex("ChildId");

                    b.ToTable("TestEntities");
                });

            modelBuilder.Entity("EFPlusTest.TestEntityChild", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Data")
                        .HasColumnType("text");

                    b.Property<bool>("TriggerHit")
                        .HasColumnType("boolean");

                    b.HasKey("Id");

                    b.ToTable("TestChildEntities");
                });

            modelBuilder.Entity("EFPlusTest.TestEntity", b =>
                {
                    b.HasOne("EFPlusTest.TestEntityChild", "Child")
                        .WithMany()
                        .HasForeignKey("ChildId");
                });
#pragma warning restore 612, 618
        }
    }
}
