﻿//#define InMemory
#define PostgresDb

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using EntityFrameworkCore.Triggers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Z.EntityFramework.Plus;

//using Z.BulkOperations;
//using Z.EntityFramework.Plus;

namespace EFPlusTest
{
    internal class Program
    {
        public const int Iterations = 1000;
#if PostgresDb
        public const string DBString = "User Id=postgres;Password=Phx2O!9;Host=localhost;Port=5432;Database=testDb";

#endif

#if InMemory
        private static string testSeed = "init";
#endif
        private static ITriggers _triggersHandle;

        private static void Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();

#if InMemory
            serviceCollection.AddEntityFrameworkInMemoryDatabase()
                .AddDbContext<TestContext>(cfg =>
                    cfg.UseInMemoryDatabase(testSeed));
#endif

#if PostgresDb
            serviceCollection.AddDbContext<TestContext>(cfg =>
                cfg.UseNpgsql(DBString));
#endif
            serviceCollection.AddTriggers();
            var provider = serviceCollection.BuildServiceProvider();
#if InMemory
            BatchDeleteManager.InMemoryDbContextFactory = () => provider.CreateScope().ServiceProvider.GetService<TestContext>();
            BatchUpdateManager.InMemoryDbContextFactory = () => provider.CreateScope().ServiceProvider.GetService<TestContext>();
#endif

            using (var outerScope = provider.CreateScope())
            {
                _triggersHandle = outerScope.ServiceProvider.GetService<ITriggers>();
                _triggersHandle.Inserting.Add(Inserting);
                _triggersHandle.Updating.Add(Updating);
                _triggersHandle.Deleting.Add(Deleting);

#if PostgresDb
                using (var migrationScope = outerScope.ServiceProvider.CreateScope())
                {
                    var context = migrationScope.ServiceProvider.GetService<TestContext>();
                    context.Database.Migrate();
                }
#endif

                PerformTest(BulkInsertSaveAfterEach, "BulkInsertSaveAfterEach", outerScope.ServiceProvider);

                PerformTest(BulkInsertSaveAfterAll, "BulkInsertSaveAfterAll", outerScope.ServiceProvider);

                PerformTest(BulkInsertEfplus, "BulkInsertSaveAfterAll", outerScope.ServiceProvider);

                PerformTest(BulkUpdateSaveAfterEach, "BulkUpdateSaveAfterEach", outerScope.ServiceProvider);

                PerformTest(BulkUpdateSaveAfterAll, "BulkUpdateSaveAfterAll", outerScope.ServiceProvider);

                PerformTest(BulkUpdateEFPlus, "BulkUpdateEFPlus", outerScope.ServiceProvider);

                PerformTest(BulkDeleteSaveAfterEach, "BulkDeleteSaveAfterEach", outerScope.ServiceProvider);

                PerformTest(BulkDeleteSaveAfterAll, "BulkDeleteSaveAfterAll", outerScope.ServiceProvider);

                PerformTest(BulkDeleteEfPlus, "BulkDeleteEfPlus", outerScope.ServiceProvider);
            }

            Console.ReadKey();
        }

        private static void ConfirmSuccess(IServiceProvider serviceProvider, string testName)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                var context = scope.ServiceProvider.GetService<TestContext>();
                var failed = context.TestEntities.Any(x => !x.TriggerHit);
                failed |= context.TestChildEntities.Any(x => !x.TriggerHit);

                Console.WriteLine($"Test {testName} has {(failed ? "FAILED" : "SUCCEEDED")}");
            }
        }

        private static void PerformTest(Action<TestContext> testAction, string testName, IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.CreateScope())
            {
#if InMemory
                testSeed = testName;
#endif
#if PostgresDb
                using (var resetScope = serviceProvider.CreateScope())
                {
                    var context = resetScope.ServiceProvider.GetService<TestContext>();

                    context.RemoveRange(context.TestEntities);
                    context.RemoveRange(context.TestChildEntities);

                    _triggersHandle.Deleting.Remove(Deleting);
                    context.SaveChanges();
                    _triggersHandle.Deleting.Add(Deleting);
                }
#endif
                var startTime = DateTime.Now;
                Console.WriteLine($"Beginning test {testName} at {startTime:O}");
                testAction.Invoke(scope.ServiceProvider.GetService<TestContext>());
                var endTime = DateTime.Now;
                Console.WriteLine($"Test Completed at {endTime:O} total time {(endTime - startTime):G}");
            }
            ConfirmSuccess(serviceProvider, testName);
        }

        private static void Inserting(IInsertingEntry<object, DbContext> obj)
        {
            if (obj.Entity is TestEntity entity)
            {
                entity.TriggerHit = true;
            }

            if (obj.Entity is TestEntityChild entity2)
            {
                entity2.TriggerHit = true;
            }
        }

        private static void Updating(IUpdatingEntry<object, DbContext> obj)
        {
            if (obj.Entity is TestEntity entity)
            {
                entity.TriggerHit = true;
            }

            if (obj.Entity is TestEntityChild entity2)
            {
                entity2.TriggerHit = true;
            }
        }

        private static void Deleting(IDeletingEntry<object, DbContext> obj)
        {
            Debug.WriteLine($"Simulated Workload {obj.Entity}");
        }

        private static void BulkInsertSaveAfterEach(TestContext testContext)
        {
            for (int ix = 0; ix < Iterations; ix++)
            {
                testContext.TestEntities.Add(new TestEntity() { Data = "TestData", Child = new TestEntityChild() { Data = "ASD" } });
                testContext.SaveChanges();
            }
        }

        private static void BulkInsertSaveAfterAll(TestContext testContext)
        {
            for (int ix = 0; ix < Iterations; ix++)
            {
                testContext.TestEntities.Add(new TestEntity() { Data = "TestData", Child = new TestEntityChild() { Data = "ASD" } });
            }
            testContext.SaveChanges();
        }

        private static void BulkInsertEfplus(TestContext testContext)
        {
            testContext.TestEntities.BulkInsert(EntitiesToInsert());
            IEnumerable<TestEntity> EntitiesToInsert()
            {
                for (int ix = 0; ix < Iterations; ix++)
                {
                    yield return new TestEntity() { Data = "TestData", Child = new TestEntityChild() { Data = "ASD" } };
                }
            }
        }

        private static void SetupUpdateDeleteTest(TestContext testContext)
        {
            _triggersHandle.Inserting.Remove(Inserting);
            BulkInsertSaveAfterAll(testContext);
            _triggersHandle.Inserting.Add(Inserting);
        }

        private static void BulkUpdateSaveAfterEach(TestContext testContext)
        {
            SetupUpdateDeleteTest(testContext);

            foreach (var entity in testContext.TestEntities.ToList())
            {
                entity.Data = "New Data";
                entity.Child.Data = "NewData";
                testContext.SaveChanges();
            }
        }

        private static void BulkUpdateSaveAfterAll(TestContext testContext)
        {
            SetupUpdateDeleteTest(testContext);

            foreach (var entity in testContext.TestEntities.ToList())
            {
                entity.Data = "New Data";
                entity.Child.Data = "NewData";
            }
            testContext.SaveChanges();
        }

        private static void BulkUpdateEFPlus(TestContext testContext)
        {
            SetupUpdateDeleteTest(testContext);

            testContext.TestEntities.Update(x => new TestEntity() { Data = "New Data" });
            testContext.TestChildEntities.Update(x => new TestEntityChild() { Data = "NewData" });
        }

        private static void BulkDeleteSaveAfterAll(TestContext testContext)
        {
            SetupUpdateDeleteTest(testContext);

            testContext.RemoveRange(testContext.TestEntities);
            testContext.RemoveRange(testContext.TestChildEntities);

            testContext.SaveChanges();
        }

        private static void BulkDeleteSaveAfterEach(TestContext testContext)
        {
            SetupUpdateDeleteTest(testContext);

            foreach (var entity in testContext.TestEntities.ToList())
            {
                testContext.Remove(entity);
                testContext.Remove(entity.Child);
                testContext.SaveChanges();
            }
        }

        private static void BulkDeleteEfPlus(TestContext testContext)
        {
            SetupUpdateDeleteTest(testContext);

            testContext.TestEntities.Delete();
            testContext.TestChildEntities.Delete();
        }
    }

    public class TestContext : DbContextWithTriggers
    {
        public TestContext(DbContextOptions options, IServiceProvider serviceProvider) : base(serviceProvider, options)
        {
        }

#if PostgresDb

        public TestContext(DbContextOptions options) : base(null, options)
        {
        }

        public TestContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(Program.DBString);
            base.OnConfiguring(optionsBuilder);
        }

#endif

        public DbSet<TestEntity> TestEntities { get; set; }
        public DbSet<TestEntityChild> TestChildEntities { get; set; }
    }

    public class TestEntity
    {
        [Key] public long Id { get; set; }
        public string Data { get; set; }
        public TestEntityChild Child { get; set; }

        public bool TriggerHit { get; set; }
    }

    public class TestEntityChild
    {
        [Key] public long Id { get; set; }
        public string Data { get; set; }

        public bool TriggerHit { get; set; }
    }
}